<?php
// Сервисы, главная:
if (!isset($array_url[0])) {
    $title = "dpos.space | Сайт с сервисами для DPOS блокчейнов Steem, Golos, Viz и Whaleshares";
    $meta_keywords = "steem, golos, Viz, whaleshares, клиент, сайт, сервисы, проекты, dpos";
    $meta_description = "Сайт с сервисами для DPOS блокчейнов Golos, Steem, Viz и Whaleshares. Просмотр профиля, фид лента, бекап постов и другое.";
    $h1 = "Сервисы для блокчейнов Steem, Golos, Viz и Whaleshares";
    $description = '<p>Создатель сервисов - незрячий программист Денис Скрипник (@denis-skripnik в любом из трёх блокчейнов)</p>';
$footer_text = "работать с блокчейнами Steem, Golos, Viz и Whaleshares. Здесь вы найдёте несколько сервисов, функционал которых увеличиваются, да и число сервисов растёт." . $chain . " в удобном виде.";
	} // Конец условия для главной